# Manual de uso

La aplicación *Wikilist* es una aplicación de almacenamiento de contactos de
prensa.

## Roles y acceso

### Roles

*Wikilist* dispone de tres roles principales para controlar el acceso a los
datos internos. Existen tres roles:

- *admin*: Puede leer, eliminar y editar contactos. Además, puede crear nuevas
  credenciales editar los registros existentes.
- *editor*: Puede leer, eliminar y editar contactos.
- *reader*: Sólo se le permite leer los contactos.

### Credenciales

Como se explica en el apartado anterior, sólo usuarios y usuarias con rol
*admin* pueden crear nuevos credenciales.

Por defecto, *Wikilist* crea un usuario con los siguientes datos de acceso:

- Email: `admin@admin.com`
- Password: `pass`

Este usuario es un usuario provisional creado para la puesta en marcha del
servicio. Debe utilizarse para crear el usuario administrador de la aplicación,
y **debe ser eliminado después** para evitar que personas sin permiso lo
utilicen (al ser público) para acceder a la aplicación.

### Acceso

Para acceder a la aplicación es necesario disponer de credenciales de usuario
creadas por parte de un administrador.

Al acceder a la aplicación el se redirecciona a una pantalla de acceso que pide
el email y el password. Al rellenar los campos y pulsar el botón `Login` se
accede a la aplicación en caso de que las credenciales sean correctas.

Si las credenciales son incorrectas un error aparece en la pantalla y el
acceso no se permite.

### Creación y edición de credenciales

Quien tenga permisos de administración dispone de acceso a la pestaña de
credenciales en la parte superior de la pantalla en el botón `Users`.

Una vez ahí pueden editarse las credenciales de **todas** las personas con
acceso a la aplicación por lo que se recomienda actuar con extrema cautela.

La pantalla de credenciales permite filtrar por rol utilizando el menú lateral
(o superior, si la pantalla es estrecha).

Para **crear** nuevas credenciales existe el botón `Add User`. Al presionarlo
se abre un formulario con los campos necesarios para registrar las
credenciales.  Al guardarlas comprobará si todas las credenciales cumplen las
características exigidas (más de 7 caracteres por contraseña, que la
confirmación de la contraseña coincida con la contraseña, etc) y visualiza un
mensaje de error si estas no se cumplen. Si se cumplen, el formulario se limpia
para facilitar la creación de otras credenciales.

Para **editar** las credenciales es necesario el botón de edición situado en
cada una de las filas de la tabla de visualización de credenciales. El
formulario que se muestra a continuación es el mismo que el de creación de
credenciales, pero en éste **no es necesario introducir la contraseña si ésta no
quiere editarse**.

> Nota: En el formulario de edición de credenciales la contraseña aparece vacía
> pero no significa que esa sea la contraseña actual. La contraseña actual no
> puede consultarse. Además, en la base de datos se almacena encriptada, por lo
> que no puede leerse bajo ninguna circunstancia.

Para **eliminar** credenciales es necesario seleccionarlas en la tabla
principal marcando el *checkbox* lateral y después pulsando el icono de
eliminación marcado con una papelera. Esto abre una pantalla de confirmación
para no eliminar credenciales de forma accidental.

## Gestión de contactos

La gestión de contactos funciona de forma muy similar a la gestión de
credenciales pero aporta más opciones.

En primer lugar, apartado de contactos es accesible por cualquier persona con
acceso a la aplicación, pero sus capacidades se limitan en función del rol
asociado a la persona usuaria.

El rol de edición y administración disponen de las mismas capacidades en el
apartado de gestión de contactos: todas. Mientras que el rol de edición no
dispone de capacidad para editar de ningún modo los registros de la aplicación.

Los contactos pueden **editarse**, **crearse** y **eliminarse** del mismo modo
que las credenciales.

Adicionalmente, en el apartado de contactos también pueden realizarse otras
acciones.

Para **visualizar** un contacto es necesario hacer click en el botón `View
contact`, indicado con el icono de un ojo, en el contacto que se quiera
visualizar.

Para **exportar** los contactos es necesario seleccionar previamente los que se
quieran exportar y pulsar el botón `Export selected as` indicado con un icono
de descarga. Al pulsarlo, de forma similar a al eliminar contactos, una ventana
de confirmación se abre para permitir la selección del formato de salida. Las
opciones son:

- **Coma Separated Values (CSV)**: Exporta los datos en un formato compatible
  con hojas de cálculo. No exporta los idiomas del contacto.
- **Electronic Business Card (VCARD)**: Exporta los datos en un formato
  compatible con otras herramientas de gestión de contactos como gestores de
  email o aplicaciones móviles. No incluye los idiomas ni la dirección del
  contacto.
- **Printable stickers**: Visualiza los contactos de forma que puedan
  imprimirse en etiquetas para correo postal. Una vez en pantalla pueden
  imprimirse utilizando los botones de impresión del navegador web (normalmente
  en Archivo-Imprimir o el acceso rápido `Ctrl+p`). Incluye el nombre completo,
  la organización y la dirección del contacto. Si la dirección no estuviese
  registrada, añade un espacio para introducirla manualmente después.

También es posible **enviar un email** los contactos seleccionados mediante el
botón `Compose to selected` indicado con el icono de un sobre. Este botón
disparará la aplicación que la persona usuaria tenga registrada en su sistema
como gestor de correo electrónico y creará un borrador con los contactos
seleccionados como copia oculta (con el fin de preservar su privacidad). Si
alguno de los contactos seleccionado no tiene un email asociado será ignorado.

> Nota: Es necesaria una buena configuración para que la opción de enviar un
> email funcione correctamente. Algunos sistemas operativos indican qué
> aplicación usar automáticamente. En función de la aplicación que se utilice
> es posible que esta funcionalidad no funcione como se espera.

Para facilitar la navegación, la aplicación dispone de un apartado de
**filtrado** de contactos. El botón `Filter result` se encarga de esto. Al
pulsarlo se despliega un menú con diferentes opciones de búsqueda. Una vez
seleccionadas o descartadas las secciones de interés, el botón `Filter`
filtrará por los criterios seleccionados. El botón `Restore` restaura los
cambios realizados en el filtro a los de la última búsqueda y el botón `Cancel`
limpia y cierra el filtro.

> Nota: El botón cancel limpia todos los campos menos el de categoría, porque
> la categoría es un campo navegable con el menú lateral.

## Edición de contactos

Los contactos disponen de más campos que las credenciales y la mayor parte son
opcionales con el objetivo de aportar flexibilidad de uso.

Los campos de categoría y organización son campos especiales obligatorios.
Ambos sugieren algunos de los valores ya registrados para facilitar la elección
pero permiten crear nuevos campos.

En el caso de la categoría, la categoría creada aparece posteriormente en el
menú lateral de la pantalla principal de los contactos. Si quiere eliminarse
alguna categoría es necesario editar los contactos que la tengan y moverlos a
otra. Las categorías en sí mismas no pueden editarse de forma independiente, ya
que no existen como tales.
