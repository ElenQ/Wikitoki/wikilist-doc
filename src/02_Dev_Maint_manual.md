# Manual técnico

La aplicación se diseña con el objetivo de ser sencilla de mantener y poner en
funcionamiento. Sin embargo, es necesario tener en cuenta un par de conceptos
para un mantenimiento correcto.

## Prerequisitos

Para ejecutar la aplicación es necesario disponer de un entorno de ejecución de
Java. Se recomienda OpenJDK en su versión 8 o superior. Para instalarlo en un
entorno Linux basado en Debian:

    apt-get install default-jre

## Instalación

La aplicación se distribuye como un fichero
[JAR](https://en.wikipedia.org/wiki/JAR_(file_format)) que incluye todas las
dependencias necesarias internamente (se conoce este formato como `uberjar`)
por lo que no es necesario instalar ningún software adicional.

Para instalar la aplicación basta con situar en fichero JAR donde quiera
contenerse.

> NOTA: La aplicación se entrega como un fichero JAR, pero el código fuente que
> lo genera también es público y accesible. El código debe ser compilado para
> obtener una aplicación ejecutable. Este proceso se detalla en el documento
> README.md del código fuente del proyecto.

## Puesta en marcha


### Ejecución

Para ejecutar la aplicación es suficiente con ejecutar este comando:

    java -jar wikilist.jar

Sin embargo, al intentar acceder la aplicación fallará por no encontrar la base
datos correspondiente y la aplicación se ejecutará automáticamente en el puerto
3000.

Para editar estas opciones es necesario ajustar las siguientes variables de
entorno al valor que interese:

    PORT=3000
    DATABASE_URL="jdbc:sqlite:Wikilist_Database.db?foreign_keys=on;"

> IMPORTANTE: en la variable `DATABASE_URL` es necesario mantener el apartado
> `?foreign_keys=on` y `jdbc:sqlite:` para que funcione correctamente.

Se recomienda crear un pequeño script de inicialización con estos contenidos:

``` bash
#!/bin/bash
DATABASE_URL=jdbc:sqlite:wikilist.db?foreign_keys=on
PORT=8080
java -jar wikilist.jar
```

Y utilizarlo como ejecutor dejando la aplicación en segundo plano.

### Configuración del servidor

Para configurar completamente la aplicación se recomienda seguir el tutorial
del framework utilizado, [consultable en este enlace](http://www.luminusweb.net/docs/deployment.html#fronting_with_nginx).

Se recomienda configurar un certificado LetsEncrypt, tal y como se cuenta en el
enlace mencionado, [en este otro, donde se explica de forma más sencilla](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-debian-9).


## Copias de seguridad

Debido a que la aplicación usa un fichero SQLite para guardar los datos
realizar copias de seguridad es extremadamente sencillo. Basta con copiar la
base datos señalada en el apartado de ejecución.

Si quisiera ejecutarse la aplicación con otra base de datos de entrada, basta
con cambiar la variable `DATABASE_URL` como en el apartado de ejecución.

Se recomienda crear un script de backup que con cierta periodicidad cree copias
de la base de datos. En su modo más sencillo el script podría ser algo similar
a esto:

``` bash
cp wikilist.db backups/`date +"%Y%m%dT%H%M"`.db
```

## Logs

La aplicación crea una carpeta de logs automáticamente en el lugar de la
ejecución. En su interior se encuentran todos los mensajes enviados por la
aplicación. Si en algún momento se necesitase comprobar algún error, ese sería
el lugar donde investigar. Estos logs rotan automáticamente.
