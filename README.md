# Wikilist user and maintainer manual

This repository contains the Wikilist user and maintainer manual. Visit the
repository of the project [here](https://gitlab.com/ElenQ/Wikitoki/wikilist).


## License

All the content (`src` folder and the resulting `pdf` file) is distributed
under the terms of the GNU Free Documentation License.

### Third party software

The project makes use of the documentation template for ElenQ Technology,
distributed under the terms of the Apache 2.0 license.
