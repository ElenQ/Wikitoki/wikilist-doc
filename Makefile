# Mini Makefile for the moment, consider moving to a different tool

C 			= pandoc -N --latex-engine=xelatex

TEMPLATE 	= template.latex

TARGETBOOK	= Wikilist_Docs.pdf

METABOOK	= book.yaml

CHANGELOG	= changes.tex

SRC			= $(sort $(wildcard src/*.md))

book: $(TARGETBOOK)

$(TARGETBOOK): $(SRC) $(TEMPLATE) $(METABOOK)
	git for-each-ref --format="%(refname:short) & %(*authordate:short) & %(subject) \\\\" refs/tags > $(CHANGELOG)
	$(C) --template=$(TEMPLATE) $(SRC) $(METABOOK) -o $(TARGETBOOK) --metadata=book:true
clean:
	rm -f $(TARGETBOOK) $(CHANGELOG)

.PHONY: book article clean
